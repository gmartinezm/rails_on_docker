FROM ruby:2.7

LABEL maintainer="German"

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -yqq && apt-get install -yqq --no-install-recommends \
	nodejs
RUN apt-get install -yqq --no-install-recommends yarn

COPY Gemfile* /usr/src/app/
WORKDIR /usr/src/app
RUN bundle install
RUN yarn install --check-files
RUN rails webpacker:install


CMD ["bin/rails", "s", "-b", "0.0.0.0"]

